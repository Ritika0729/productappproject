﻿using BookingApi.Model;

namespace BookingApi.Repository
{
    public interface IBookingRepository
    {
        bool AddToCart(Booking booking);
        List<Booking> GetCartByUserId(int id);
        bool Delete(DeleteCart deleteCart);
    }
}
