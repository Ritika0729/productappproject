﻿namespace BookingApi.Model
{
    public class DeleteCart
    {
        public int UserId { get; set; }        
        public int ProductId { get; set; }
    }
}
