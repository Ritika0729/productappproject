import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/service/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user:User;

  constructor(private userService:UserService,private router:Router) { 
    console.log('within register Componants');
    this.user= new User()
  }

  ngOnInit(): void {
    // this.registerUser();
  }
  registerUser(user:User) { 
    // this.user.name="msd"; 
    // this.user.password="msd";
    // this.user.location="Pune";
    // this.user.email="msd@gmail.com";
    // this.user.isblocked=false;
    console.log(user)
    this.userService.RegisterUser(user).subscribe(res=>{
      if(res){
        console.log("Registration Sucess");
        Swal.fire(
          'User Registration',
          'User Registraion success',
          'success'
        )
        this.router.navigateByUrl("/login")
        
        }
      else{
        console.log("Regitraion failed");
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!',
        })
      }
    });
  }

}
